# Pull official image
FROM python:3.9.2-alpine3.12 as python-base

# Set working directory
WORKDIR /app

# Set environment variables
ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

# Install system dependencies
RUN apk add --update --no-cache postgresql-client
RUN apk update && apk add python3-dev \
        gcc libc-dev linux-headers postgresql-dev \
        libffi-dev openssl-dev cargo
RUN apk add musl-dev freetype libpng libjpeg-turbo freetype-dev libpng-dev libjpeg-turbo-dev

# Install python dependencies and packages
RUN pip install --upgrade pip
COPY requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt


EXPOSE 8000

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
