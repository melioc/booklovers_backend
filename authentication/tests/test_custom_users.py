from django.test import TestCase
from django.contrib.auth import get_user_model

CustomUser = get_user_model()


class CustomUserManagerTest(TestCase):
    """ test of the user personalization and its methods"""
    def test_create_user(self):
        """ test the creation of a simple user"""
        user = CustomUser.objects.create_user(
            email="test@test.com",
            username="test",
            password="test123456"
        )

        self.assertEqual(user.email, "test@test.com")
        self.assertEquals(user.username, "test")
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)

    def test_create_superuser(self):
        """ Test of the creation of an administrator """
        admin_user = CustomUser.objects.create_superuser(
            email='admin@test.com',
            username='admin',
            password='monsupermodp2310',
        )
        self.assertEqual(admin_user.email, 'admin@test.com')
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
