from django.test import TestCase
from authentication.serializers import CustomUserDetailsSerializer


class TestAuthenticationSerializer(TestCase):
    """ Class to test getting user information with the serializer"""
    def test_valid_custom_serializer(self):
        """ Test valid custom user information"""
        valid_serializer_data = {
            "username": "test",
            "email": "test@test.com",
            "first_name": "toto",
            "last_name": "dupont"
        }

        serializer = CustomUserDetailsSerializer(data=valid_serializer_data)
        self.assertTrue(serializer.is_valid)
        if serializer.is_valid():
            self.assertEqual(serializer.validated_data, valid_serializer_data)
        self.assertEqual(serializer.data, valid_serializer_data)
        self.assertEqual(serializer.errors, {})

    def test_invalid_email_custom_serializer(self):
        """ test serializer when no email register when creating account"""
        invalid_serializer_data = {
            "username": "test",
            "first_name": "toto",
            "last_name": "dupont"
        }

        serializer = CustomUserDetailsSerializer(data=invalid_serializer_data)
        self.assertTrue(serializer.is_valid)
        if serializer.is_valid() is False:
            self.assertEqual(serializer.validated_data, {})
        self.assertEqual(serializer.data, invalid_serializer_data)
        self.assertEqual(
            serializer.errors,
            {
                'email':
                    ['This field is required.']
            }
        )

    def test_invalid_username_custom_serializer(self):
        """ test serializer when no username register when creating account"""
        invalid_serializer_data = {
            "email": "test@test.com",
            "first_name": "toto",
            "last_name": "dupont"
        }

        serializer = CustomUserDetailsSerializer(data=invalid_serializer_data)
        self.assertTrue(serializer.is_valid)
        if serializer.is_valid() is False:
            self.assertEqual(serializer.validated_data, {})
        self.assertEqual(serializer.data, invalid_serializer_data)
        self.assertEqual(
            serializer.errors, {
                'username': ['This field is required.']
            }
        )
