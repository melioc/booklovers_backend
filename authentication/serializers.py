from rest_framework import serializers
from django.contrib.auth import get_user_model


CustomUser = get_user_model()


# User  details serializer
class CustomUserDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ("pk", "username", "email", "first_name", "last_name")
