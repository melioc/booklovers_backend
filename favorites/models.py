from django.db import models
from django.contrib.auth import get_user_model
from books.models import Book


CustomUser = get_user_model()


class Favorite(models.Model):
    """ Favorites Table"""
    status = models.CharField(max_length=55, blank=False, default="To Read")

    book = models.ForeignKey(
        Book, related_name="books_favorite", on_delete=models.CASCADE
    )
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    class Meta:
        unique_together = (('book_id', 'user_id'),)

    def __str__(self):
        return str(self.user)
