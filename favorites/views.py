from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from rest_framework.generics import (
    ListAPIView,
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from books.models import Book

from .models import Favorite
from .serializers import FavoritesSerializer, MyFavoriteBooksSerializer


CustomUser = get_user_model()


class FavoritesBooksView(ListCreateAPIView):
    """ Class to get or create favorites """
    permission_class = [IsAuthenticated]
    serializer_class = FavoritesSerializer

    def get_queryset(self):
        user = self.request.user
        return Favorite.objects.filter(user=user)

    def create(self, request, *args, **kwargs):
        # allow adding book to favorites
        user = request.user
        book = get_object_or_404(Book, pk=request.data["book"])
        stat = request.data["status"]

        new_favorite = Favorite(user=user, book=book, status=stat)
        new_favorite.save()
        serializer = FavoritesSerializer(new_favorite)
        return Response(serializer.data)

    def get(self, request, *args, **kwargs):
        # get favorites data by user, and return all the boo's information
        books = Favorite.objects.filter(user=request.user)
        serializer = MyFavoriteBooksSerializer(books, many=True)
        return Response(serializer.data)


class FavoritesUpdateView(RetrieveUpdateDestroyAPIView):
    """ class to allow getting, updating or deleting the favorites' data"""
    permission_class = [IsAuthenticated]
    serializer_class = FavoritesSerializer
    queryset = Favorite.objects.all()


class ShelfFavoritesListView(ListAPIView):
    """ Get favorites by shelf"""
    permission_classes = [IsAuthenticated]
    serializer_class = MyFavoriteBooksSerializer

    def get_queryset(self):
        queryset = Favorite.objects.all()
        status_query = self.request.query_params.get('status')
        if status_query is not None:
            queryset = queryset.filter(
                user=self.request.user,
                status__iexact=status_query
            )
        return queryset
