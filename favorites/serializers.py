from rest_framework.serializers import ModelSerializer
from .models import Favorite
from books.serializers import BookSerializer


class FavoritesSerializer(ModelSerializer):
    """ Serializer class of the Favorite model"""
    class Meta:
        model = Favorite
        fields = ('id', 'user', 'book', 'status')


class MyFavoriteBooksSerializer(ModelSerializer):
    """
    Serializer class to retrieve books' information
    from Book model for users
    """
    book = BookSerializer()

    class Meta:
        model = Favorite
        fields = ('id', 'user', 'book', 'status',)
