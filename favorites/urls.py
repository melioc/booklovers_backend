from django.urls import path
from .views import (
    FavoritesBooksView,
    FavoritesUpdateView,
    ShelfFavoritesListView
)


urlpatterns = [
    path('', FavoritesBooksView.as_view(),),
    path('<int:pk>', FavoritesUpdateView.as_view(),),
    path('shelves/', ShelfFavoritesListView.as_view(),),
]
