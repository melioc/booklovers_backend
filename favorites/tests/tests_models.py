from django.test import TestCase
from django.contrib.auth import get_user_model
from ..models import Favorite

from books.models import Book

CustomUser = get_user_model()


class TestFavoriteModel(TestCase):
    """
    class to test that model returns all data correctly
    """
    def setUp(self):
        # set up  non modified objects used by tests
        self.user = CustomUser.objects.create_user(
            id=1,
            email="test@test.com",
            username="test",
            password="test1234"
        )
        self.book = Book.objects.create(
            id=35,
            title="les tests unitaires",
            description="sgherherreer",
            published_date="2015",
            image_url="http://books.fr"
        )

    def test_fav_model(self):
        # test to look if we get the good data registered in favorite model
        fav = Favorite(
            status="to read",
            book=self.book,
            user=self.user
        )
        fav.save()

        self.assertEqual(fav.status, "to read")
        self.assertEqual(fav.book.id, 35)
        self.assertEqual(fav.user.id, 1)
