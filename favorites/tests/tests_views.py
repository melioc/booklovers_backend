from django.test import TestCase
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APIRequestFactory, force_authenticate
from ..views import FavoritesBooksView
from books.models import Book
from ..models import Favorite

CustomUser = get_user_model()


class TestFavoritesBooksView(TestCase):
    """ Class to test the methods of the FavoritesBooksView class"""
    def setUp(self):
        # set up non modified objects used by the tests

        self.factory = APIRequestFactory()
        self.user = CustomUser.objects.create_user(
            id=1,
            email="test@test.com",
            username="test",
            password="test12345"
        )
        self.book = Book.objects.create(
            id=35,
            title="les tests unitaires",
            description="sgherherreer",
            published_date="2015",
            image_url="http://books.fr")

        self.view = FavoritesBooksView.as_view()

    def test_create_favorites_valid_data(self):
        # testing the creation of a favorites
        self.assertEqual(Favorite.objects.all().count(), 0)
        req = self.factory.post(
            "favorites/",
            {
                "status": "to read",
                "book": self.book.id,
                "user": self.user
            }
        )

        force_authenticate(req, user=self.user)
        response = self.view(req)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Favorite.objects.all().count(), 1)

    def test_get_favorites(self):
        # testing that we can get correctly favorites' data
        # we create a Favorite
        fav = Favorite(
            id=1, status="to read", book=self.book, user=self.user
        )
        fav.save()

        # Request to get the favorites' data
        req = self.factory.get(
            "favorites/"
        )

        force_authenticate(req, user=self.user)
        response = self.view(req, pk=1)
        response.render()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.content,
                         b'['
                         b'{"id":1,'
                         b'"user":1,'
                         b'"book":'
                         b'{"id":35,'
                         b'"title":"les tests unitaires",'
                         b'"description":"sgherherreer",'
                         b'"published_date":"2015",'
                         b'"authors":[],'
                         b'"image_url":"http://books.fr"},'
                         b'"status":"to read"}]'
                         )

    def test_create_favorites_invalid_data(self):
        # testing the creation of a favorites with invalid token
        self.assertEqual(Favorite.objects.all().count(), 0)
        req = self.factory.post(
            "favorites/",
            {
                "status": "to read",
                "book": self.book,
                "user": self.user
            }
        )
        response = self.view(req)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Favorite.objects.all().count(), 0)

    def test_create_favorite_with_existing_data(self):
        # test adding an existed favorite again
        fav = Favorite(status="to read", book=self.book, user=self.user)
        fav.save()

        self.assertEqual(Favorite.objects.all().count(), 1)
        req = self.factory.post(
            "favorites/",
            {
                "status": "to read",
                "book": 2,
                "user": self.user
            }
        )

        force_authenticate(req, user=self.user)
        response = self.view(req)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(Favorite.objects.all().count(), 1)
