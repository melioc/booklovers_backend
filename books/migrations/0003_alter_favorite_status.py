# Generated by Django 3.2 on 2021-08-02 14:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0002_auto_20210802_1301'),
    ]

    operations = [
        migrations.AlterField(
            model_name='favorite',
            name='status',
            field=models.CharField(default='To Read', max_length=55),
        ),
    ]
