from django.contrib.auth import get_user_model
from rest_framework.filters import SearchFilter
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.permissions import AllowAny


from .models import Book, Author, Category
from .serializers import (
    BookSerializer,
    AuthorSerializer,
    CategorySerializer,
)

CustomUser = get_user_model()


class ListBookView(ListAPIView):
    """ List of the books in the db """
    permission_classes = [AllowAny]
    queryset = Book.objects.all()
    serializer_class = BookSerializer


class SearchBooksView(ListAPIView):
    """
    Class to get the results of a search query
    """
    permission_classes = [AllowAny]
    search_fields = [
        'title', 'authors__author_name', 'categories__category_name'
    ]
    filter_backends = (SearchFilter,)
    queryset = Book.objects.all()
    serializer_class = BookSerializer


class DetailBookView(RetrieveAPIView):
    # Displaying book's details
    permission_classes = [AllowAny]
    queryset = Book.objects.all()
    serializer_class = BookSerializer


class AuthorListView(ListAPIView):
    """
    Display the list of all the authors with
    the id of the books linked to each
    """
    permission_classes = [AllowAny]
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer


class AuthorDetailView(RetrieveAPIView):
    # Getting Author's detail
    permission_classes = [AllowAny]
    serializer_class = BookSerializer

    def get_queryset(self):
        """
        personalization of the query for getting
        author's name through many to many relations
        """
        author = Author.objects.get(pk=self.kwargs['pk'])
        return Book.objects.filter(authors=author)


class CategoryListView(ListAPIView):
    # Displaying Categories' list
    permission_class = [AllowAny]
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CategoryDetailView(RetrieveAPIView):
    # getting the categories' details
    permission_class = [AllowAny]
    serializer_class = BookSerializer

    def get_queryset(self):
        category = Category.objects.get(pk=self.kwargs['pk'])
        return Book.objects.filter(categories=category)
