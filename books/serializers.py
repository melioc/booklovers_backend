from rest_framework import serializers
from .models import Book, Category, Author


# Get authors' information and link Author table to Book table
class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ('id', 'author_name', 'books')


# Get categories' information and linking Book table
class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'category_name', 'books')


# Get books' information
class BookSerializer(serializers.ModelSerializer):
    authors = AuthorSerializer(many=True)

    class Meta:
        model = Book
        fields = (
            'id',
            'title',
            'description',
            'published_date',
            'authors',
            'image_url',
        )
