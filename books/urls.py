from django.urls import path
from .views import (
    ListBookView,
    DetailBookView,
    AuthorListView,
    AuthorDetailView,
    CategoryListView,
    CategoryDetailView,
    SearchBooksView,
)

urlpatterns = [
    path('books-list', ListBookView.as_view()),
    path('<int:pk>', DetailBookView.as_view()),
    path('authors-list', AuthorListView.as_view()),
    path('author/<int:pk>', AuthorDetailView.as_view()),
    path('cat-list', CategoryListView.as_view()),
    path('cat/<int:pk>', CategoryDetailView.as_view()),
    path('searchBooks/', SearchBooksView.as_view()),
]
