#Bookslovers

# Features

Django et django rest framework with Python 3.9
Postgres13
Unittest for backend tests
Docker compose for easier development


# Development
The only dependencies for this project should be docker and docker-compose. When cloning, do for the first time:

- Enter your directory:

    - `cd booklovers_backend/`

- Create a secret key, tape in your terminal:

    - `openssl rand -hex 32`


- Create a .env file with that syntax:
    - `DEBUG=True`
    - `DJANGO_ALLOWED_HOSTS=localhost 127.0.0.1 [::1]`
    - `POSTGRES_ENGINE=django.db.backends.postgresql` 
    - `SECRET_KEY=secret_key (copy/paste your terminal's code)`
    - `POSTGRES_USER=username`
    - `POSTGRES_PASSWORD=password`
    - `POSTGRES_HOST=booklovers_db`
    - `POSTGRES_PORT=5432`
    - `POSTGRES_DB=database_name`

- Build your docker:
    - `docker-compose up -d --build`

- Create your tables:
    - `docker-compose exec booklovers python manage.py migrate`
    
- Create superuser:
    - `docker-compose exec booklovers python manage.py createsuperuser`

- Insert data in db:
    - `docker-compose exec booklovers python manage.py initdb`

- To run tests:
    - `docker-compose exec booklovers coverage run test`
    
  